<?php
namespace lib\JhWechat;


/**
 * 用户管理
 * @Auth: JH <hu@lunaz.cn>
 * Class User
 * @package lib\JhWechat
 * @link http://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140839
 */
class User extends Base
{

    /**
     * @param array $options
     * @param string $access_token
     */
    function __construct(array $options = array(), $access_token = '')
    {
        if ($access_token) {
            parent::$accessToken = $access_token;
        }
        if ($options) {
            parent::$config = $options;
        }
        parent::__construct();
    }

    /**
     * 获取用户列表
     *
     * @param string $next_openid 拉取公众号关注用户开始索引
     *
     * @return mixed|array
     * <pre>
     * return
     * Array(
     *      [total] => 2 //关注该公众账号的总用户数
     *      [count] => 2 //本次拉取的OPENID个数，最大值为10000
     *      [data] => Array(
     *              [openid] => Array( //列表数据，OPENID的列表
     *                      [0] => OPENID1
     *                      [1] => OPENID2
     *                  )
     *              )
     *      [next_openid] => NEXT_OPENID //拉取列表的最后一个用户的OPENID
     * )
     * </pre>
     * @throws Exception
     */
    function getUserList($next_openid = '')
    {
        $param = $this->getAccessToken();
        $next_openid && $param['next_openid'] = $next_openid;
        return $this->http(
            $this->makeUrl('/user/get', $param)
        );
    }

    /**
     * 获取用户基本信息（包括UnionID机制）
     *
     * @param string $open_id
     * @param string $lang zh_CN 简体，zh_TW 繁体，en 英语
     *
     * @return mixed|array
     * <pre>
     * return
     * Array(
     *      [subscribe] => 1 //否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。
     *      [openid] => o6_bmjrPTlm6_2sgVt7hMZOPfL2M
     *      [nickname] => Band //用户的昵称
     *      [sex] => 1 //用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
     *      [language] => zh_CN //用户的语言
     *      [city] => 广州 //用户所在城市
     *      [province] => 广东 //用户所在省份
     *      [country] => 中国 //用户所在国家
     *      [headimgurl] => http://wx.qlogo.cn/mmopen/xxxx/0
     *     //用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
     *      [subscribe_time] => 1382694957 //用户关注时间戳
     *      [unionid] => o6_bmasdasdsad6_2sgVt7hMZOPfL //公众号对粉丝的备注
     *      [remark] => //只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
     *      [groupid] => 0 //用户所在的分组ID
     *      [tagid_list] => Array( //用户被打上的标签ID列表
     *                          [0] => 128
     *                          [1] => 2
     *                      )
     * )
     * </pre>
     * @throws Exception
     */
    function getUserInfo($open_id = '', $lang = 'zh_CN')
    {
        $param = $this->getAccessToken() + array('openid' => $open_id, 'lang' => $lang);
        return $this->http(
            $this->makeUrl('/user/info', $param)
        );
    }

    /**
     * 批量获取用户基本信息
     *
     * @param array $open_id_list
     * <pre>
     * $open_id_list
     * Array(
     *      [0] => Array(
     *                  [openid] => otvxTs4dckWG7imySrJd6jSi0CWE
     *                  [lang] => zh-CN
     *             ),
     *      [1] => Array(
     *                  [openid] => otvxTs_JZ6SEiP0imdhpi50fuSZg
     *                  [lang] => zh-CN
     *             )
     * )
     * </pre>
     *
     * @return mixed
     * @throws Exception
     */
    function getBatchUserInfo(array $open_id_list)
    {
        $open_id_list = array_chunk($open_id_list, 100);
        $result = array();
        foreach ($open_id_list as $li) {
            $query = $this->http(
                $this->makeUrl('/user/info/batchget', $this->getAccessToken()),
                array('user_list' => $li)
            );
            if (isset($query['user_info_list'])) {
                if (empty($result)) {
                    $result = $query['user_info_list'];
                } else {
                    foreach ($query['user_info_list'] as $uli) {
                        $result[] = $uli;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * 设置用户备注名
     *
     * @param string $open_id
     * @param string $remark 新的备注名，长度必须小于30字符
     *
     * @return mixed
     * @throws Exception
     */
    function updateRemark($open_id, $remark)
    {
        return $this->http(
            $this->makeUrl('/user/info/updateremark', $this->getAccessToken()),
            array('openid' => $open_id, 'remark' => $remark)
        );
    }

    /**
     * 创建标签
     * 一个公众号，最多可以创建100个标签。
     *
     * @param string $tag_name
     *
     * @return mixed|array
     * <pre>
     * return
     * Array(
     *      [tag] => Array(
     *                  [id] => 134 //标签id
     *                  [name] => 广东 //标签名，UTF8编码
     *               )
     * )
     * </pre>
     * @throws Exception
     */
    function addUserTag($tag_name)
    {
        return $this->http(
            $this->makeUrl('/tags/create', $this->getAccessToken()),
            array(
                'tag' => array(
                    'name' => $tag_name
                )
            )
        );
    }

    /**
     * 获取公众号已创建的标签
     * @return mixed|array
     * <pre>
     * return
     * Array(
     *      [tags] => Array(
     *                      [0] => Array(
     *                                  [id] => 1 //id为0/1/2这三个是系统默认的标签
     *                                  [name] => 黑名单
     *                                  [count] => 0 //该标签下标记了多少粉丝
     *                              ),
     *                      [1] => Array(
     *                                  [id] => 2
     *                                  [name] => 星标组
     *                                  [count] => 0
     *                              ),
     *                      [2] => Array(
     *                                  [id] => 127
     *                                  [name] => 广东
     *                                  [count] => 5
     *                              )
     *              )
     * )
     * </pre>
     * @throws Exception
     */
    function getTagsAll()
    {
        return $this->http(
            $this->makeUrl('/tags/get', $this->getAccessToken())
        );
    }

    /**
     * 更新标签
     *
     * @param int $tag_id
     * @param string $name
     *
     * @return mixed|array
     * @throws Exception
     */
    function updateTag($tag_id, $name)
    {
        return $this->http(
            $this->makeUrl('/tags/update', $this->getAccessToken()),
            array('tag' => array('id' => $tag_id, 'name' => $name))
        );
    }

    /**
     * 删除标签
     *
     * @param int $tag_id
     *
     * @return mixed
     * @throws Exception
     */
    function delTag($tag_id)
    {
        return $this->http(
            $this->makeUrl('/tags/delete', $this->getAccessToken()),
            array('tag' => array('id' => $tag_id))
        );
    }

    /**
     * 获取标签下粉丝列表
     *
     * @param int $tag_id
     * @param string $index_openid
     *
     * @return mixed
     * <pre>
     * return
     * Array(
     *      [count] => 2
     *      [data] => Array(
     *                    [openid] => Array(
     *                                  [0] => ocYxcuAEy30bX0NXmGn4ypqx3tI0
     *                                  [1] => ocYxcuBt0mRugKZ7tGAHPnUaOW7Y
     *                                )
     *                )
     *      [next_openid] => ocYxcuBt0mRugKZ7tGAHPnUaOW7Y
     * )
     * </pre>
     * @throws Exception
     */
    function getTagUserList($tag_id, $index_openid = '')
    {
        return $this->http(
            $this->makeUrl('/user/tag/get', $this->getAccessToken()),
            array('tagid' => $tag_id, 'next_openid' => $index_openid)
        );
    }

    /**
     * 批量为用户打标签
     *
     * @param array $open_id_list
     * <pre>
     * array(
     *      [0] => ocYxcuAEy30bX0NXmGn4ypqx3tI0
     *      [1] => ocYxcuBt0mRugKZ7tGAHPnUaOW7Y
     * )
     * </pre>
     * @param int $tag_id
     *
     * @return mixed
     * @throws Exception
     */
    function setBatchTagUser(array $open_id_list, $tag_id)
    {
        return $this->http(
            $this->makeUrl('/tags/members/batchtagging', $this->getAccessToken()),
            array(
                'openid_list' => $open_id_list,
                'tagid' => $tag_id
            )
        );
    }

    /**
     * 批量为用户取消标签
     *
     * @param array $open_id_list
     * @param int $tag_id
     *
     * @return mixed
     * @throws Exception
     */
    function unBatchTagUser(array $open_id_list, $tag_id)
    {
        return $this->http(
            $this->makeUrl('/tags/members/batchuntagging', $this->getAccessToken()),
            array(
                'openid_list' => $open_id_list,
                'tagid' => $tag_id
            )
        );
    }

    /**
     * @param string $open_id
     *
     * @return mixed|array
     * <pre>
     * Array(
     *      [tagid_list] => Array(
     *                          [0] => 134
     *                          [1] => 2
     *                      )
     * )
     * </pre>
     * @throws Exception
     */
    function getUserTags($open_id = '')
    {
        return $this->http(
            $this->makeUrl('/tags/getidlist', $this->getAccessToken()),
            array(
                'openid' => $open_id
            )
        );
    }
}
