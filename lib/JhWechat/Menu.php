<?php
namespace lib\JhWechat;

/**
 * 自定义菜单
 * @Auth: JH <hu@lunaz.cn>
 * Class Menu
 * @package lib\JhWechat
 */
class Menu extends Base
{

    /**
     * @param array $options
     * @param string $access_token
     */
    function __construct( array $options = array(), $access_token = '' )
    {
        if ($access_token) {
            parent::$accessToken = $access_token;
        }
        if ($options) {
            parent::$config = $options;
        }
        parent::__construct();
    }

    /**
     * @return mixed
     * @throws Exception
     */
    function getMenu()
    {
        return $this->http(
            $this->makeUrl( '/menu/get', $this->getAccessToken() )
        );
    }

    /**
     * @param array $menu
     * @return mixed
     * @throws Exception
     */
    function setMenu( array $menu )
    {
        return $this->http(
            $this->makeUrl( '/menu/create', $this->getAccessToken() ),
            $menu
        );
    }

    /**
     * @return mixed
     * @throws Exception
     */
    function delMenu()
    {
        $result = $this->http(
            $this->makeUrl( '/menu/delete', $this->getAccessToken() )
        );
        return $result;
    }

    /**
     * 设置个性自定义菜单（针对指定条件用户设置菜单）
     * @param array $menu
     * @param array $condition
     * @pre $condition = Array(
     * [tag_id] => 2 //标签的id
     * [sex] => 1 //性别：男（1）女（2），不填则不做匹配
     * [country] => 中国 //国家信息
     * [province] => 广东 //省份信息
     * [city] => 广州 //城市信息
     * [client_platform_type] => 2 //系统型号：IOS(1), Android(2),Others(3)
     * [language] => zh_CN //1、简体中文 "zh_CN" 2、繁体中文TW "zh_TW" 3、繁体中文HK "zh_HK" 4、英文 "en" 5、印尼 "id" 6、马来 "ms" 7、西班牙 "es" 8、韩国 "ko" 9、意大利 "it" 10、日本 "ja" 11、波兰 "pl" 12、葡萄牙 "pt" 13、俄国 "ru" 14、泰文 "th" 15、越南 "vi" 16、阿拉伯语 "ar" 17、北印度 "hi" 18、希伯来 "he" 19、土耳其 "tr" 20、德语 "de" 21、法语 "fr"
     * )
     * @return mixed {"menuid":"208379533"}
     */
    function setConditionMenu( array $menu, array $condition = array() )
    {
        return $this->http( $this->makeUrl( '/menu/addconditional', $this->getAccessToken() ),
            array_merge( $menu, $condition )
        );
    }

    /**
     * 删除条件个性目录
     * @param int $menu_id
     * @return mixed
     * @throws Exception
     */
    function delConditionMenu( $menu_id )
    {
        return $this->http( $this->makeUrl( '/menu/delconditional', $this->getAccessToken() ),
            array('menuid' => $menu_id)
        );
    }

    /**
     * 获取指定关注者显示目录
     * @param string $user 微信号|OpenID
     * @return mixed
     * @throws Exception
     */
    function findUserMenu( $user )
    {
        return $this->http(
            $this->makeUrl( '/menu/trymatch', $this->getAccessToken() ),
            array('user_id' => $user)
        );
    }

    /**
     * @return array
     */
    function getMenuType()
    {
        return [
            'click' => [
                'name' => '点击推事件',
                'desc' => '用户点击click类型按钮后，微信服务器会通过消息接口推送消息类型为event的结构给开发者（参考消息接口指南），并且带上按钮中开发者填写的key值，开发者可以通过自定义的key值与用户进行交互'
            ],
            'view' => [
                'name' => '跳转URL',
                'desc' => '用户点击view类型按钮后，微信客户端将会打开开发者在按钮中填写的网页URL，可与网页授权获取用户基本信息接口结合，获得用户基本信息。'
            ],
            'scancode_push' => [
                'name' => '扫码推事件',
                'desc' => '用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后显示扫描结果（如果是URL，将进入URL），且会将扫码的结果传给开发者，开发者可以下发消息。'
            ],
            'scancode_waitmsg' => [
                'name' => '扫码推事件且弹出“消息接收中”提示框',
                'desc' => '用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后，将扫码的结果传给开发者，同时收起扫一扫工具，然后弹出“消息接收中”提示框，随后可能会收到开发者下发的消息。'
            ],
            'pic_sysphoto;' => [
                'name' => '弹出系统拍照发图',
                'desc' => '用户点击按钮后，微信客户端将调起系统相机，完成拍照操作后，会将拍摄的相片发送给开发者，并推送事件给开发者，同时收起系统相机，随后可能会收到开发者下发的消息。'
            ],
            'pic_photo_or_album' => [
                'name' => '弹出拍照或者相册发图',
                'desc' => '用户点击按钮后，微信客户端将弹出选择器供用户选择“拍照”或者“从手机相册选择”。用户选择后即走其他两种流程。'
            ],
            'pic_weixin' => [
                'name' => '弹出微信相册发图器',
                'desc' => '用户点击按钮后，微信客户端将调起微信相册，完成选择操作后，将选择的相片发送给开发者的服务器，并推送事件给开发者，同时收起相册，随后可能会收到开发者下发的消息。'
            ],
            'location_select' => [
                'name' => '弹出地理位置选择器',
                'desc' => '用户点击按钮后，微信客户端将调起地理位置选择工具，完成选择操作后，将选择的地理位置发送给开发者的服务器，同时收起位置选择工具，随后可能会收到开发者下发的消息。'
            ],
            'media_id' => [
                'name' => '下发消息（除文本消息）',
                'desc' => '用户点击media_id类型按钮后，微信服务器会将开发者填写的永久素材id对应的素材下发给用户，永久素材类型可以是图片、音频、视频、图文消息。请注意：永久素材id必须是在“素材管理/新增永久素材”接口上传后获得的合法id。'
            ],
            'view_limited' => [
                'name' => '跳转图文消息URL',
                'desc' => '用户点击view_limited类型按钮后，微信客户端将打开开发者在按钮中填写的永久素材id对应的图文消息URL，永久素材类型只支持图文消息。请注意：永久素材id必须是在“素材管理/新增永久素材”接口上传后获得的合法id。'
            ]
        ];
    }


}
