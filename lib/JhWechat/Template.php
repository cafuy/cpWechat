<?php
namespace lib\JhWechat;

/**
 * 模板消息
 * @Auth: JH <hu@lunaz.cn>
 * Class Template
 * @package lib\JhWechat
 */
class Template extends Base
{

    /**
     * @param array $options
     * @param string $access_token
     */
    function __construct( array $options = array(), $access_token = '' )
    {
        if ($access_token) {
            parent::$accessToken = $access_token;
        }
        if ($options) {
            parent::$config = $options;
        }
        parent::__construct();
    }

    function getIndustry()
    {
        return $this->http(
            $this->makeUrl('/template/get_industry',$this->getAccessToken())
        );
    }
}
