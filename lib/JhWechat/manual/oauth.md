### 微信网页授权

[官方文档说明](https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140842) 

#### 使用方法

提供两种方法
- onlyOpenId(...) 只获取用户OPENID，只跳转无感知
- fullUserInfo(...) 需要用户授权，获取用户详细信息

```php
//step 1
//进入微信登录专用页面，跳转到微信服务器
$wechat = new Wechat( $config );
//将自动跳转到微信服务器，微信服务器将带着code参数重定向到callback地址 http://xxx.com/wechat_login/?code=CODE&state=STATE
$wechat->oauth->onlyOpenId( 'http://xxx.com/wechat_login' );
--- OR ---
// 需要用户授权后才会重定向到callback地址
$wechat->oauth->fullUserInfo( 'http://xxx.com/wechat_login' );

//step 2
$result = $wechat->oauth->userInfo($_GET['code']);
print_r($result);
Array(
     [access_token] => ACCESS_TOKEN
     [expires_in] => 7200
     [refresh_token] => REFRESH_TOKEN
     [openid] => OPENID
     [scope] => SCOPE
 )
 --- OR ---
 Array(
      [access_token] => ACCESS_TOKEN
      [expires_in] => 7200
      [refresh_token] => REFRESH_TOKEN
      [openid] => OPENID
      [scope] => SCOPE
      [nickname] => NICKNAME
      [sex] => 1
      [province] => PROVINCE
      [city] => CITY
      [country] => COUNTRY
      [headimgurl] => http://wx.qlogo.cn/mmopen/g3MonUZtNHk/46
      [privilege] => Array(
                         [0] => PRIVILEGE1
                         [1] => PRIVILEGE2
                     )
      [unionid] => o6_bmasdasdsad6_2sgVt7hMZOPfL
  )
```