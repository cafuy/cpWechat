<?php
namespace lib\JhWechat;

/**
 * 素材管理
 * @Auth: JH <hu@lunaz.cn>
 * Class Media
 * @package lib\JhWechat
 */
class Media extends Base
{
    private $type = array('image', 'voice', 'video', 'thumb');

    /**
     * @param array $options
     * @param string $access_token
     */
    function __construct( array $options = array(), $access_token = '' )
    {
        if ($access_token) {
            parent::$accessToken = $access_token;
        }
        if ($options) {
            parent::$config = $options;
        }
        parent::__construct();
    }

    /**
     * 上传素材
     * @param string $file
     * @param bool $temporary 0|1
     * @param string $type image|voice|video|thumb
     * @param null $key
     * @return mixed
     * @throws Exception
     * @throws \Exception
     */

    function uploadMedia( $file, $type = 'image | voice | video | thumb', $temporary = true, $key = null )
    {
        $uri = $temporary ? '/media/upload' : '/material/add_material';
        $this->checkType( $type );
        $result = $this->http(
            $this->makeUrl( $uri, array('access_token' => $this->getToken(), 'type' => $type) ),
            $file
        );
        if ($key) {
            return isset( $result[$key] ) ? $result[$key] : false;
        }
        return $result;
    }

    /**
     * 获取临时素材
     * @param string $media_id
     * @return mixed
     * @throws Exception
     */
    function getTemporary( $media_id )
    {
        return $this->http(
            $this->makeUrl( '/media/get', array('access_token' => $this->getToken(), 'media_id' => $media_id) )
        );
    }

    /**
     *上传图文消息内的图片，返回URL
     * @param string $img_path
     * @return false|URL
     * @throws Exception
     */
    function uploadImg( $img_path )
    {
        $ext = parent::getFileInfo( $img_path, 'extension' );
        if (!in_array( $ext, array('jpg', 'png') )) {
            throw new Exception( "上传图片不支持{$ext}格式" );
        }
        $result = $this->http(
            $this->makeUrl( '/media/uploadimg', $this->getAccessToken() ),
            array('media' => $img_path)
        );
        if (isset( $result['url'] )) {
            return $result['url'];
        }
        return false;
    }

    /**
     * @param array|string $video
     * @param string $title
     * @param string $introduction
     * @return mixed
     * @throws Exception
     */
    function uploadVideo( $video, $title = '视频素材的标题', $introduction = '视频素材的描述' )
    {
        if (is_array( $video ) && isset( $data['path'], $data['title'], $data['introduction'] )) {
            list( $file, $title, $introduction ) = $data;
        } else {
            $file = $video;
        }

        $this->addMultiFormData(
            array(
                'description' => $this->makeJson(
                    array(
                        'title' => $title,
                        'introduction' => $introduction
                    )
                )
            )
        );
        return $this->uploadMedia( $file, 'video', false );
    }

    /**
     * 获取素材列表
     * @param string $type
     * @param int $offset
     * @param int $count
     * @return mixed|false
     * @throws Exception
     */
    function getList( $type, $offset = 0, $count = 20 )
    {
        $this->checkType( $type );
        $result = $this->http(
            $this->makeUrl( '/material/batchget_material', $this->getAccessToken() ),
            array(
                'type' => $type,
                'offset' => $offset,
                'count' => min( 20, $count )
            )
        );
        return $result;
    }

    /**
     * 获取素材总数
     * @return mixed|array
     * @throws Exception
     */
    function getTotal()
    {
        return
            $this->http(
                $this->makeUrl( '/material/get_materialcount', $this->getAccessToken() )
            );
    }

    /**
     * 获取永久素材
     * @param string $media_id
     * @return mixed
     * @throws Exception
     */
    function get( $media_id )
    {
        return $this->http(
            $this->makeUrl( '/material/get_material', $this->getAccessToken() ),
            array('media_id' => $media_id)
        );
    }

    /**
     * 删除永久素材
     * @param string $media_id
     * @return mixed
     * @throws Exception
     */
    function del( $media_id )
    {
        return $this->http(
            $this->makeUrl( '/material/del_material', $this->getAccessToken() ),
            array('media_id' => $media_id)
        );
    }

    private function checkType( $type )
    {
        if (!in_array( $type, $this->type )) {
            throw new Exception( "不支持{$type}类型素材" );
        }
    }
}
