<?php
namespace lib\JhWechat;

/**
 * 多客服
 * @Auth: JH <hu@lunaz.cn>
 * Class KfManager
 * @package lib\JhWechat
 * @link http://kf.qq.com/faq/120911VrYVrA160126Nvi6NN.html[申请开通说明]
 */
class KfManager extends Base
{
    /**
     * @param array $options
     * @param string $access_token
     */
    function __construct( array $options = array(), $access_token = '' )
    {
        if ($access_token) {
            parent::$accessToken = $access_token;
        }
        if ($options) {
            parent::$config = $options;
        }
        parent::__construct();
    }

    /**
     * 添加客服帐号
     * @param string $account
     * @param string $nick
     * @return bool
     * @throws Exception
     */
    function add( $account, $nick )
    {
        return $this->http(
            $this->makeUrl( '/customservice/kfaccount/add', $this->getAccessToken(), parent::API_BASE ),
            array(
                'kf_account' => $account,
                'nickname' => $nick
            )
        );
    }

    /**
     * 邀请绑定客服帐号
     * @param string $account 客服编号@公众号微信号
     * @param string $invite 客服个人微信号
     * @return bool
     * @throws Exception
     */
    function invite( $account, $invite )
    {
        return $this->http(
            $this->makeUrl( '/customservice/kfaccount/inviteworker', $this->getAccessToken(), parent::API_BASE ),
            array(
                'kf_account' => $account,
                'invite_wx' => $invite
            )
        );
    }

    /**
     * 修改客服帐号
     * @param string $account
     * @param string $nick
     * @return bool
     * @throws Exception
     */
    function update( $account, $nick )
    {
        return $this->http(
            $this->makeUrl( '/customservice/kfaccount/update', $this->getAccessToken(), parent::API_BASE ),
            array(
                'kf_account' => $account,
                'nickname' => $nick
            )
        );
    }

    /**
     * 删除客服帐号
     * @param string $account
     * @return bool
     * @throws Exception
     */
    function del( $account )
    {
        return $this->http(
            $this->makeUrl(
                '/customservice/kfaccount/del',
                $this->getAccessToken() + array(
                    'kf_account' => $account,
                ),
                parent::API_BASE
            )
        );
    }

    /**
     * 获取所有客服账号
     * @return false|array
     * <pre>
     * Array(
     *      [0] => Array(
     *              [kf_account] => test1@test
     *              [kf_nick] => ntest1
     *              [kf_id] => 1001
     *              [kf_headimgurl] => http://mmbiz.qpic.cn/mmbiz/4whpOmw/0
     *              [kf_wx] => xxx //客服个人微信号
     *      ),
     *      [1] => Array(
     *              [kf_account] => test2@test
     *              [kf_nick] => ntest2
     *              [kf_id] => 1002
     *              [kf_headimgurl] => http://mmbiz.qpic.cn/mmbiz/4whpV1V/0
     *              [kf_wx] => xxx //客服个人微信号
     *      )
     * )
     * </pre>
     * @throws Exception
     */
    function getList()
    {
        $result = $this->http(
            $this->makeUrl( '/customservice/getkflist', $this->getAccessToken() )
        );
        if (isset( $result['kf_list'] )) {
            return $result['kf_list'];
        }
        return false;
    }

    /**
     * 获取在线客服接待信息
     * @return false|array
     * <pre>
     * Array(
     *       [0] => Array(
     *                  [kf_account] => test1@test
     *                  [status] => 1
     *                  [kf_id] => 1001
     *                  [auto_accept] => 0
     *                  [accepted_case] => 1
     *              ),
     *       [1] => Array(
     *                  [kf_account] => test2@test
     *                  [status] => 1
     *                  [kf_id] => 1002
     *                  [auto_accept] => 0
     *                  [accepted_case] => 2
     *              )
     * )
     * </pre>
     * @throws Exception
     */
    function getOnline()
    {
        $result = $this->http(
            $this->makeUrl( '/customservice/getonlinekflist', $this->getAccessToken() )
        );
        if (isset( $result['kf_online_list'] )) {
            return $result['kf_online_list'];
        }
        return false;
    }

    function createSession( $to_openid, $kf )
    {
        return $this->http(
            $this->makeUrl( '/customservice/kfsession/create', $this->getAccessToken(), parent::API_BASE ),
            array(
                'kf_account' => $kf,
                'openid' => $to_openid
            )
        );
    }

    function closeSession( $close_openid, $kf )
    {
        return $this->http(
            $this->makeUrl( '/customservice/kfsession/close', $this->getAccessToken(), parent::API_BASE ),
            array(
                'kf_account' => $kf,
                'openid' => $close_openid
            )
        );
    }

    /**
     *
     * @param string $open_id
     * @return array|false
     * <pre>
     * Array(
     *      [createtime] => 会话接入的时间
     *      [kf_account] => 正在接待的客服，为空表示没有人在接待
     * )
     * </pre>
     * @throws Exception
     */
    function getSession( $open_id = '' )
    {
        return $this->http(
            $this->makeUrl(
                '/customservice/kfsession/getsession',
                $this->getAccessToken() + array('openid' => $open_id),
                parent::API_BASE
            )
        );
    }

    /**
     *
     * @param int $start_time
     * @param int $end_time
     * @param int $page
     * @param int $page_size
     * @return false|array
     * <pre>
     * Array(
     *      [0] => Array(
     *                  [openid] => oFPNJs1LlyoTgFODkVWJkWgZ7Aqs
     *                  [opercode] => 2002 //操作码，2002（客服发送信息），2003（客服接收消息）
     *                  [text] => 11
     *                  [time] => 1466420396
     *                  [worker] => 1000@test
     *      )
     *      [1] => Array(
     *                  [openid] => oFPNJs1LlyoTgFODkVWJkWgZ7Aqs
     *                  [opercode] => 2003 //操作码，2002（客服发送信息），2003（客服接收消息）
     *                  [text] => 33
     *                  [time] => 1466420433
     *                  [worker] => 1000@test
     *      )
     * )
     * </pre>
     * @throws Exception
     */
    function messageRecord( $start_time, $end_time, $page = 1, $page_size = 50 )
    {
        $result = $this->http(
            $this->makeUrl( '/customservice/msgrecord/getrecord', $this->getAccessToken(), parent::API_BASE ),
            array(
                'starttime' => $start_time,
                'endtime' => $end_time,
                'pageindex' => $page,
                'pagesize' => $page_size
            )
        );
        if (isset( $result['recordlist'] )) {
            return $result['recordlist'];
        }
        return false;
    }

    /**
     * 上传设置客服帐号的头像
     * @param string $img
     * @param string $account
     * @return bool
     * @throws Exception
     */
    function setAvatar( $img, $account )
    {
        return $this->http(
            $this->makeUrl( '/customservice/kfaccount/uploadheadimg',
                array('access_token' => $this->getToken(), 'kf_account' => $account),
                parent::API_BASE
            ),
            $img
        );
    }

}
