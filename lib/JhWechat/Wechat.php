<?php
namespace lib\JhWechat;

/**
 * @Auth: JH <hu@lunaz.cn>
 * Class Wechat
 * @package lib\JhWechat
 * @property Menu $menu
 * @property KfManager $kf
 * @property Article $article
 * @property Media $media
 * @property Message $message
 * @property Oauth $oauth
 * @property Qrcode $qr
 * @property Server $server
 * @property Template $tem_msg
 * @property Tools $tools
 * @property User $user
 */
class Wechat extends Base
{

    /**
     * @param array $options
     * @param string $access_token
     */
    function __construct( array $options = array(), $access_token = '' )
    {
        if ($access_token) {
            parent::$accessToken = $access_token;
        }
        parent::$config = $options;
        parent::__construct();
    }

    function __get( $name )
    {
        static $class = array();
        if (isset( $class[$name] )) {
            return $this->{$name} = $class[$name];
        }

        switch ($name) {
            case 'article':
                $class[$name] = new Article();
                break;
            case 'media':
                $class[$name] = new Media();
                break;
            case 'menu':
                $class[$name] = new Menu();
                break;
            case  'kf':
                $class[$name] = new KfManager();
                break;
            case 'message':
                $class[$name] = new Message();
                break;
            case 'oauth':
                $class[$name] = new Oauth();
                break;
            case 'qr':
                $class[$name] = new Qrcode();
                break;
            case 'server':
                $class[$name] = new Server();
                break;
            case 'tem_msg':
                $class[$name] = new Template();
                break;
            case 'tools':
                $class[$name] = new Tools();
                break;
            case 'user':
                $class[$name] = new User();
                break;
        }
        return $this->{$name} = $class[$name];
    }
}
