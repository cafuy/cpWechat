<?php
namespace lib\JhWechat;

/**
 * 主动通信及群发
 * @Auth: JH <hu@lunaz.cn>
 * Class Message
 * @package lib\JhWechat
 */
class Message extends Base
{

    private $content = array();

    /**
     * @param array $options
     * @param string $access_token
     */
    function __construct( array $options = array(), $access_token = '' )
    {
        if ($access_token) {
            parent::$accessToken = $access_token;
        }
        if ($options) {
            parent::$config = $options;
        }
        parent::__construct();
    }

    /**
     * 发送文本消息
     * @param string $content
     * @return $this
     * @throws Exception
     */
    function text( $content )
    {
        $this->content = array(
            'msgtype' => 'text',
            'text' => array(
                'content' => $content
            )
        );
        return $this;
    }

    /**
     * 发送图片消息
     * @param string $media_id
     * @return $this
     * @throws Exception
     */
    function image( $media_id )
    {
        $this->content = array(
            'msgtype' => 'image',
            'image' => array(
                'media_id' => $media_id
            )
        );
        return $this;
    }

    /**
     * @param string $media_id
     * @return $this
     * @throws Exception
     */
    function voice( $media_id )
    {
        $this->content = array(
            'msgtype' => 'voice',
            'voice' => array(
                'media_id' => $media_id
            )
        );
        return $this;
    }

    /**
     * @param string $media_id
     * @param string $thumb_media_id
     * @param string $title
     * @param string $description
     * @return $this
     * @throws Exception
     */
    function video( $media_id, $thumb_media_id, $title, $description )
    {
        $this->content = array(
            'msgtype' => 'video',
            'video' => array(
                'media_id' => $media_id,
                "thumb_media_id" => $thumb_media_id,
                "title" => $title,
                "description" => $description
            )
        );
        return $this;
    }

    /**
     * 发送音乐消息
     * @param string $thumb_media_id
     * @param string $title
     * @param string $description
     * @param string $music_url
     * @param string $hq_url
     * @return $this
     * @throws Exception
     */
    function music( $thumb_media_id, $title, $description, $music_url, $hq_url )
    {
        $this->content = array(
            'msgtype' => 'music',
            'music' => array(
                "thumb_media_id" => $thumb_media_id,
                "title" => $title,
                "description" => $description,
                'musicurl' => $music_url,
                'hqmusicurl' => $hq_url
            )
        );
        return $this;
    }

    /**
     * @param array $news_list
     * @return $this
     */
    function news( array $news_list = array('title' => '', 'description' => '', 'url' => '', 'picurl' => '') )
    {
        $this->content = array(
            'msgtype' => 'news',
            'news' => array(
                'articles' => $news_list
            )
        );

        return $this;
    }

    function mpNews( $media_di )
    {
        $this->content = array(
            'msgtype' => 'mpnews',
            'mpnews' => array(
                'media_id' => $media_di
            )
        );
        return $this;
    }

    function Card( $card_id, $card_ext )
    {
        /**
         * @todo 未完成
         */
    }


    /**
     * 发送信息给指定用户
     * 允许发送类型 wxcard|mpnews|music|video|voice|image|text|news
     * @param string $to_open_id
     * @param string $kf_account 以某个客服帐号来发消息
     * @return bool|mixed
     * @throws Exception
     */
    function send( $to_open_id, $kf_account = '' )
    {
        $this->checkContent();
        $this->content['touser'] = $to_open_id;
        if ($kf_account && stripos( '@', $kf_account )) {
            $this->content['customservice'] = array('kf_account' => $kf_account);
        }
        return $this->http(
            $this->makeUrl( '/message/custom/send', $this->getAccessToken() ),
            $this->content
        );
    }

    /**
     *
     * @param bool $is_to_all
     * @param int $tag_id
     * @return bool
     * @throws Exception
     */
    function sendAll( $is_to_all = true, $tag_id = 2 )
    {
        $this->checkContent( true );
        $this->redress();
        $this->content['filter'] = array(
            'is_to_all' => $is_to_all,
            'tag_id' => $tag_id
        );
        $result = $this->http(
            $this->makeUrl( '/message/mass/sendall', $this->getAccessToken() ),
            $this->content
        );
        if (isset( $result['msg_id'] )) {
            return $result['msg_id'];
        }
        return false;
    }

    /**
     *
     * @param array $open_id_list
     * @return false|msg_id
     * @throws Exception
     */
    function sendList( array $open_id_list )
    {
        $this->checkContent( true );
        $this->content['touser'] = $open_id_list;
        $result = $this->http(
            $this->makeUrl( '/message/mass/send', $this->getAccessToken() ),
            $this->content
        );
        if (isset( $result['msg_id'] )) {
            return $result['msg_id'];
        }
        return false;
    }

    /**
     * 删除群发
     * @param int $msg_id
     * @return bool
     * @throws Exception
     */
    function del( $msg_id )
    {
        return $this->http(
            $this->makeUrl( '/message/mass/delete', $this->getAccessToken() ),
            array('msg_id' => $msg_id)
        );
    }

    /**
     * 查询群发消息发送状态
     * @param int $msg_id
     * @return array|bool|mixed
     * @throws Exception
     */
    function get( $msg_id = 1 )
    {
        return $this->http(
            $this->makeUrl( '/message/mass/get', $this->getAccessToken() ),
            array('msg_id' => $msg_id)
        );
    }

    /**
     * 预览群发信息
     * @param string $to_open_id
     * @param null $to_account
     * @return false|msg_id
     * @throws Exception
     */
    function preview( $to_open_id = '', $to_account = null )
    {
        $this->checkContent( true );
        $this->redress();
        if (!is_null( $to_account )) {
            $this->content['towxname'] = $to_account;
        } else {
            $this->content['touser'] = $to_open_id;
        }
        $result = $this->http(
            $this->makeUrl( '/message/mass/preview', $this->getAccessToken() ),
            $this->content
        );
        if (isset( $result['msg_id'] )) {
            return $result['msg_id'];
        }
        return false;
    }

    private function checkContent( $is_more = false )
    {
        if (!$this->content) {
            throw new Exception( '缺少发送内容' );
        }
        if ($is_more && $this->content['msgtype'] == 'news') {
            throw new Exception( '不允许群发【news】类型' );
        }
    }

    private function redress()
    {
        if ($this->content['msgtype'] == 'video') {
            $this->content['msgtype'] = 'mpvideo';
            $this->content['mpvideo'] = $this->content['video'];
            unset( $this->content['video'], $this->content['mpvideo']['title'], $this->content['mpvideo']['description'] );
        }
    }
}
