# cpWechat 微信快速开发骨架
## 使用说明
### 先下载crossPHP 框架到cpWechat 同级目录
[crossPHP git@osc](http://git.oschina.net/ideaa/crossphp) 

```
1.
git clone https://git.oschina.net/ideaa/crossphp.git
2.
git clone https://git.oschina.net/cafuy/cpWechat.git
```
最终目录结构
```
./path/
       ├ crossphp
       ├ cpWechat/
                  ├ app/
                  ├ config/
                  ├ htdocs/
                           ├ wechat   ← 将与微信服务器对接接口服务器指向此入口目录
                  ├ lib/
                        ├ JhWechat ← 封装好的微信接口SDK
                  ├ log/
```
\* 要求：`cpWechat/cache` `cpWechat/log` 文件夹可写可读
### 修改微信配置文件
将 `cpWechat/config/wechat_sample.config.php` 重命名为 `wechat.config.php`
修改里面配置项为，如果多个公众号，可在同一个配置文件配置多个公众号配置，需要在入口文件加载配置时指定key, 或者复制多个入口文件，把加载配置写死也可以
```php
array(
    'wechat_one' => array(
        'token' => 'sample_key', //填写你设定的key
        'encodingAesKey' => 'geX8G97HkOoU4seSUwcvb1mxjDTiZNLx64Oe7SujuFV', //填写加密用的EncodingAESKey
        'appId' => 'sample_70ce6360243', //填写高级调用功能的app id, 请在微信开发模式后台查询
        'appSecret' => 'sample_913cd01616df4bc42e84f2908', //填写高级调用功能的密钥
        'debug' => false,
        'file_cache' => true
    ),
    'wechat_two' => array(),
    'wechat_N...' => array()
)
```
### 服务器业务层控制器
在 cpWechat/app/wechat/controllers 里面对应的控制器对应方法中尽情的快速写业务就可以了
每个控制器都对应着微信事件推送
在每个方法里调用 $this->wechat->SDK封装好的类
如`Event`控制器
```php
//用户关注公众号
    function subscribe()
    {
        $openid = $this->getOpenId(); //获取用户OPENID
        $user_info = $this->wechat->user->getUserInfo($openid); //从OPENID获取用户详细资料
        $nick = '';
        if (isset($user_info['nickname'])) {
            $nick = $user_info['nickname'];
        }
        $msg = "你好 {$nick}! 欢迎关注我们的公众号";
        //扫码场景下(关注)与(扫码)事件同时进行
        $this->scan();
        $this->server->responseText($msg); //通过同步接口返回信息给用户
    }
```
控制器：
`Event`
`Image`
`Link`
`Location`
`ShortVideo`
`Text`
`Video`
`Voice`