<?php
/**
 * @Auth: JH <hu@lunaz.cn>
 */
require '../../bootstrap.php';
$config = require '../../config/wechat.config.php';
$server = Cross\Core\Delegate::loadApp('wechat', $config);

$req = $server->getRequest();
$wechat_config = $server->getConfig()->get('wechat_one');
$wechat = new lib\JhWechat\Server($wechat_config);

if (isset($_GET['echostr'])) {

    if ($wechat->check($_GET)) {
        echo $_GET['echostr'];
    }

} elseif ($req->isPostRequest() && !empty($data = file_get_contents('php://input'))) {

    $wechat->parseData($data);
    if (isset($data['MsgType'])) {
        $controller = $wechat->msgTypeToController($data['MsgType']);
        if (isset($data['Event'])) {
            $action = $wechat->eventToAction($data['Event']);
        } else {
            $action = 'index';
        }
        $data['wechat_config'] = $wechat_config;
        try {
            $server->get("{$controller}:{$action}", $data);
        } catch (Exception $e) {
            $wechat->log($e->getMessage() . ' Trace ' . $e->getTraceAsString());
            $server->getResponse()->setResponseStatus(500)->display('exception');
        }
        return;
    }
}
//判断为非法请求
$server->getResponse()->setResponseStatus(404)->displayOver();

