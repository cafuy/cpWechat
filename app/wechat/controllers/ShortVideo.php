<?php
namespace app\wechat\controllers;

/**
 * 接收微信服务器推送普通消息（小视频）
 * @Auth: JH <hu@lunaz.cn>
 * Class ShortVideo
 * @package app\server\controllers
 */
class ShortVideo extends Server
{
    /**
     * @link http://mp.weixin.qq.com/wiki/10/79502792eef98d6e0c6e1739da387346.html#.E5.B0.8F.E8.A7.86.E9.A2.91.E6.B6.88.E6.81.AF
     */
    function index()
    {
        $media_id = $this->getParams('MediaId');
        $thumb_media_id = $this->getParams('ThumbMediaId');
        $this->server->responseText('你发来的小视频已经收到');
    }

}
