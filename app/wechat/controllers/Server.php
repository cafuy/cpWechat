<?php
namespace app\wechat\controllers;

use Cross\Core\FrameBase;
use lib\JhWechat\Wechat;

/**
 * @Auth: JH <hu@lunaz.cn>
 * Class Server
 * @package app\server\controllers
 *
 */
abstract class Server extends FrameBase
{
    protected $wechat;
    protected $server;

    function __construct()
    {
        parent::__construct();
        $this->wechat = new Wechat($this->getParams('wechat_config'));
        // 如果没有使用内置的文件缓存，在实例化wechat类时传第二个参数，将外部获取的access_token传入
        // 即配置中的 file_cache 为false时
        //$this->wechat = new Wechat($this->getParams('wechat_config'), $access_token);
        $this->server = $this->wechat->server;
        $this->server->setData($this->params);
    }

    /**
     * 安全获取参数
     * @param $key
     *
     * @return string
     */
    function getParams($key)
    {
        if (isset($this->params[$key])) {
            return $this->params[$key];
        }
        return '';
    }

    /**
     * 获取用户OPENID
     * @return string
     */
    function getOpenId()
    {
        return $this->getParams('FromUserName');
    }

    /**
     * @return mixed
     */
    abstract function index();
}
