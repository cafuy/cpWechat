<?php
namespace app\wechat\controllers;


/**
 * @Auth: JH <hu@lunaz.cn>
 * Class Event
 * @package app\server\controllers
 * @link http://mp.weixin.qq.com/wiki/2/5baf56ce4947d35003b86a9805634b1e.html
 */
class Event extends Server
{
    /**
     * @return mixed
     */
    function index()
    {

    }

    /**
     * 用户关注公众号
     */
    function subscribe()
    {
        $openid = $this->getOpenId();
        $user_info = $this->wechat->user->getUserInfo($openid);
        $nick = '';
        if (isset($user_info['nickname'])) {
            $nick = $user_info['nickname'];
        }
        $msg = "你好 {$nick}! 欢迎关注我们的公众号";
        //扫码场景下(关注)与(扫码)事件同时进行
        $this->scan();
        $this->server->responseText($msg);
    }

    /**
     * 用户取消关注
     */
    function unSubscribe()
    {
        $openid = $this->getOpenId();
        //这里可以删除数据库用户的OPENID/更新取消关注标识
    }

    /**
     * 扫码事件
     */
    function scan()
    {
        $key = $this->getParams('EventKey');
        $ticket = $this->getParams('Ticket');
        // 业务开始。。。

    }

    /**
     * 用户进入公众号上报地理位置，前提用户授权允许公众号获取其位置
     */
    function location()
    {
        /**
         * $this->params 含有以下参数
         * Latitude => 23.137466 纬度
         * Longitude => 113.352425 经度
         * Precision => 119.385040 精度
         */

        // 业务开始 。。。
    }

    /**
     * 用户点击自定义菜单（拉取消息）触发事件
     */
    function click()
    {
        $key = $this->getParams('EventKey');
        switch ($key) {
            case 'click_key1':
                // 业务
                return;
            case 'click_key2':
                return;
        }
    }

    /**
     * 用户点击自定义菜单（跳转链接）触发事件
     */
    function view()
    {
        $url = $this->getParams('EventKey');
        echo 'success';
    }
}
