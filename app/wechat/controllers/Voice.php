<?php
namespace app\wechat\controllers;

/**
 * 接收微信服务器推送普通消息（语音）
 * @Auth: JH <hu@lunaz.cn>
 * Class Voice
 * @package app\server\controllers
 */
class Voice extends Server
{
    /**
     * @link http://mp.weixin.qq.com/wiki/10/79502792eef98d6e0c6e1739da387346.html#.E8.AF.AD.E9.9F.B3.E6.B6.88.E6.81.AF
     */
    function index()
    {
        $format = $this->getParams('Format');
        $media_id = $this->getParams('MediaId');
        $voice_text = $this->getParams('Recognition'); // 需开通语音识别才有此参数
        $this->server->responseText('你发来的语音已经收到');
    }

}
