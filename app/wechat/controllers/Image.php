<?php
namespace app\wechat\controllers;

/**
 * 接收微信服务器推送普通消息（图片）
 * @Auth: JH <hu@lunaz.cn>
 * Class Image
 * @package app\server\controllers
 */
class Image extends Server
{
    /**
     * @link http://mp.weixin.qq.com/wiki/10/79502792eef98d6e0c6e1739da387346.html#.E5.9B.BE.E7.89.87.E6.B6.88.E6.81.AF
     */
    function index()
    {
        $pic_url = $this->getParams('PicUrl');
        $media_id = $this->getParams('MediaId');
        $this->server->responseText('你发来的图片已经收到');
    }
}
