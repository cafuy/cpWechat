<?php
namespace app\wechat\controllers;

/**
 * 接收微信服务器推送普通消息（地理位置）
 * @Auth: JH <hu@lunaz.cn>
 * Class Location
 * @package app\server\controllers
 */
class Location extends Server
{

    /**
     * @link http://mp.weixin.qq.com/wiki/10/79502792eef98d6e0c6e1739da387346.html#.E5.9C.B0.E7.90.86.E4.BD.8D.E7.BD.AE.E6.B6.88.E6.81.AF
     */
    function index()
    {
        $x = $this->getParams('Location_X'); //维度
        $y = $this->getParams('Location_Y'); //经度
        $s = $this->getParams('Scale'); //地图缩放大小
        $label = $this->getParams('Label'); //地理位置信息
        $this->server->responseText('正在为您定位中。。。');
    }


}
