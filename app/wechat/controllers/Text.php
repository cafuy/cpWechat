<?php
namespace app\wechat\controllers;


/**
 * 接收微信服务器推送普通消息（文本）
 * @Auth: JH <hu@lunaz.cn>
 * Class Text
 * @package app\server\controllers
 */
class Text extends Server
{

    /**
     * @link http://mp.weixin.qq.com/wiki/10/79502792eef98d6e0c6e1739da387346.html#.E6.96.87.E6.9C.AC.E6.B6.88.E6.81.AF
     */
    function index()
    {
        $content = $this->getParams('Content');
        $this->server->responseText('你发来的信息已经收到');
    }

}
