<?php
namespace app\wechat\controllers;

/**
 * 接收微信服务器推送普通消息（链接）
 * @Auth: JH <hu@lunaz.cn>
 * Class Link
 * @package app\server\controllers
 */
class Link extends Server
{

    function index()
    {
        $title = $this->getParams('Title'); //标题
        $desc = $this->getParams('Description'); //描述
        $url = $this->getParams('Url'); //url
        $this->server->responseText("感谢你为我们推荐文章\n{$title}\n我们将尽快查阅");
    }

}
