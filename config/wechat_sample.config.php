<?php
/**
 * @Auth: JH <hu@lunaz.cn>
 */

return array(
    'wechat_one' => array(
        'token' => 'sample_key', //填写你设定的key
        'encodingAesKey' => 'geX8G97HkOoU4seSUwcvb1mxjDTiZNLx64Oe7SujuFV', //填写加密用的EncodingAESKey
        'appId' => 'sample_70ce6360243', //填写高级调用功能的app id, 请在微信开发模式后台查询
        'appSecret' => 'sample_913cd01616df4bc42e84f2908', //填写高级调用功能的密钥
        'debug' => false,
        'file_cache' => true
    ),
    'wechat_two' => array(),
    'wechat_N...' => array()
);
